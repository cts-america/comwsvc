﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

using UtilClassLibrary;
using CtsClassLibrary;

namespace ComWSvc
{
    public class EMailPolling: PagingThread
    {
        public EMailPolling(int aPollingPeriod, TCtsLogger aLogger)
            : base(aPollingPeriod, aLogger)
        {
            fLogger.Write("EMailPolling.Create", TLogLevel.llFlow);
        }

        public string DefaultSender { get; set; }
        public string User
        { 
            get{ return Setup.UserName;} 
            set{ Setup.UserName = value;}
        }

        public string Password 
        { 
            get{ return Setup.Password;} 
            set{ Setup.Password = value;} 
        }

        public string Host
        {
            get { return Setup.Host; }
            set { Setup.Host = value; }
        }

        public int Port
        {
            get { return Setup.Port; }
            set { Setup.Port = value; }
        }

        public bool EnableSsl
        {
            get { return Setup.EnableSsl; }
            set { Setup.EnableSsl = value; }
        }

        private EMailSetup Setup = new EMailSetup();

        private bool Post(EMailMessage aMsg)
        {
            fLogger.Write("EMailPolling.Post", TLogLevel.llFlow);
            try
            {
                new TEmail(fLogger, Setup).Send(aMsg);
                fLogger.Write("EMailPolling.Post Sent to: " + aMsg.ToAddr, TLogLevel.llTransAction);
                return true;
            }
            catch(Exception e)
            {
                fLogger.Write("EMailPolling.Post SEND ERROR: " + e.Message
                    + " Mail Server:  " + Setup.Host
                    + " Mail Port:    " + Convert.ToString(Setup.Port)
                    + " Local Host:   " + Setup.Host
                    + " Send From:    " + aMsg.FromAddr
                    + " Send To:      " + aMsg.ToAddr
                    + " Message Text: " + aMsg.Message, TLogLevel.llException);
                return false;
            }
        }

        protected override void Event()
        {
            fLogger.Write("EMailPolling.Event", TLogLevel.llFlow);
            string Delete = "";
            string Update = "";
            try
            {
                EMailMessage Msg = new EMailMessage();

                TCtsQuery Query = new TCtsQuery("Mct_Web");
                SqlDataReader Reader = Query.ExecuteProc("dbo.spsc_GetEmails");
                while (Reader.Read())
                {
                    Msg.Subject =  (string)Reader["SUBJECT"];
                    Msg.Message =  (string)Reader["MESSAGE"];
                    Msg.ToAddr =   (string)Reader["TO"];
                    Msg.FromAddr = (string)Reader["FROM"];
                    if (Msg.FromAddr == "") { Msg.FromAddr = DefaultSender; };//data issues
                    if (Post(Msg))
                    { Delete += "<row><Uniquekey>"+(string)Reader["Uniquekey"]+"</Uniquekey></row>";}
                    else
                    { Update += "<row><Uniquekey>"+(string)Reader["Uniquekey"]+"</Uniquekey></row>";}
                    if (IsLogMsgs) { TransLog("INET", Msg.FromAddr, Msg.Message, Msg.ToAddr, "", ""); };
                }
                Query.Close();
                TCtsQuery Proc = new TCtsQuery("Mct_Web");
                Proc.ParmByName("@DeleteXml", SqlDbType.VarChar, "<Delete>" + Delete + "</Delete>");
                Proc.ParmByName("@UpDateXml", SqlDbType.VarChar, "<Update>" + Update + "</Update>");
                Proc.ExecuteProc("spsc_UpdateDeleteEMail");
                Proc.Close();
            }
            catch(Exception e)
            {
                fLogger.Write("EMailPolling.Event " + e.Message, TLogLevel.llException);
                fLogger.Write("EMailPolling.PollingTimeEvent These were sent but not deleted:" + Update, TLogLevel.llTransAction);
                fLogger.Write("EMailPolling.PollingTimeEvent These were not sent and not updated:" + Delete, TLogLevel.llTransAction);
            }
        }
    }
}
