﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UtilClassLibrary;
using CtsClassLibrary;

namespace ComWSvc
{
    public abstract class PagingThread: PollingThread
    {
        protected int Iff(bool aTest, int aTrue, int aFalse)
        { 
            if (aTest)
            { return aTrue; }
            else
            { return aFalse; }
        }

        public bool IsLogMsgs { get; set; }

        public PagingThread(int aPollingPeriod, TCtsLogger aLogger)
            : base(aPollingPeriod, aLogger){}

        protected void TransLog(string aCommand, string aWho, string aWhat, string aWhere, string aToCallNo, string aFromCallNo)
        {
            fLogger.Write("PagingThread.TransLog", TLogLevel.llFlow);
            if (!IsLogMsgs) { return; };
            string   C_Insert = @"Insert into dbo.TransLog
                    (Uniquekey, Gateway, Command, Who, What, [When], [Where], TO_CALL_NO, FROM_CALL_NO)
                     values
                    (scadmin.dbo.fnsc_GetUniqueID(newid()), 'WEB', @Command, @Who, @What, getDate(), @Where, @TO_CALL_NO, @FROM_CALL_NO)";
            TCtsQuery Query = new TCtsQuery("Mct_Switch");
            Query.ParmByName("@Command", System.Data.SqlDbType.VarChar, aCommand);
            Query.ParmByName("@Who", System.Data.SqlDbType.VarChar, aWho.Substring(0, Iff(aWho.Length > 20, 20, aWho.Length)));
            Query.ParmByName("@What", System.Data.SqlDbType.VarChar, aWhat);
            Query.ParmByName("@Where", System.Data.SqlDbType.VarChar, Iff(aWhere.Length > 15, 15, aWhere.Length));
            Query.ParmByName("@To_Call_No", System.Data.SqlDbType.VarChar, Iff(aToCallNo.Length > 10, 10, aToCallNo.Length));
            Query.ParmByName("@From_Call_No", System.Data.SqlDbType.VarChar, Iff(aFromCallNo.Length > 10, 10, aFromCallNo.Length));

            Query.ExecuteNonQuery(C_Insert);
        }

    }
}
