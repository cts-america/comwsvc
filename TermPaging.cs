﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

using UtilClassLibrary;
using CtsClassLibrary;

namespace ComWSvc
{
    class TermPaging : PagingThread
    {
        TermSetup Setup = new TermSetup();

        public TermPaging(int aPollingPeriod, TCtsLogger aLogger)
            : base(aPollingPeriod, aLogger)
        {
            fLogger.Write("TermPaging.Create", TLogLevel.llFlow);
            Setup.Baud = 9600;
            Setup.ComNumber = 2;
            Setup.BufferSize = 8192;
        }

        private bool Post(string aPager, string aServer, string aMessage)
        {
            try
            {
                TermMessage SM = new TermMessage();
                SM.PagerNo = aPager;
                SM.Message = aMessage;

                new TTerm(fLogger, Setup).Send(SM);
                return true;
            }
            catch(Exception E)
            {
                fLogger.Write("TapiPaging.Post" + E.Message, TLogLevel.llException);
                return false;
            }
        }

        protected override void Event()
        {
            fLogger.Write("TermPaging.Event", TLogLevel.llFlow);
            string Deletes = "";
            string Updates = "";
            try
            {
                TCtsQuery Query = new TCtsQuery("Mct_Web");
                SqlDataReader Reader = Query.ExecuteProc("dbo.spsc_GetTermPages");
                while (Reader.Read())
                {
                    bool fSent = Post((string)Reader["PAGER"],
                                      (string)Reader["MAIL_SERVER"],
                                      (string)Reader["MESSAGE"]);
                    
                    if (IsLogMsgs) 
                    { 
                        TransLog( fSent ? "CPage" : "XPage",
                           (string)Reader["PAGEDBY"],
                           (string)Reader["MESSAGE"],
                           (string)Reader["PAGER"],
                           (string)Reader["CallNo"],
                           (string)Reader["PAGEDBY"]);
                    }

                    if (fSent)
                    { Deletes += TXml.Encode("Uniquekey", (string)Reader["Uniquekey"]); }
                    else
                    { Updates += TXml.Encode("Uniquekey", (string)Reader["Uniquekey"]); }

                }
                TCtsQuery Proc = new TCtsQuery("Mct_Web");
                Proc.ParmByName("@DeleteXml", SqlDbType.VarChar, TXml.Encode("Delete", Deletes));
                Proc.ParmByName("@UpDateXml", SqlDbType.VarChar, TXml.Encode("Update", Updates));
                Proc.ExecuteProc("dbo.spsc_UpdateDeletePages");
                Proc.Close();

            }
            catch(Exception e)
            {
                fLogger.Write("TermPaging.Event " + e.Message, TLogLevel.llException);
                fLogger.Write("TermPaging.Event These were sent but not deleted: "+ Updates, TLogLevel.llTransAction);
                fLogger.Write("TermPaging.Event These were not sent and not updated: "+ Deletes, TLogLevel.llTransAction);
            }
        }
    }
}
