﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UtilClassLibrary;
using CtsClassLibrary;

namespace ComWSvc
{
    class TComThreadMgr : UtilClassLibrary.IThreadManagerForWinSvc
    {
        private TCtsLogger Logger;
        private EMailPolling EMail = null;
        private SmtpPaging SmtpPage = null;
        private SnppPaging SnppPage = null;
        private TapiPaging TapiPage = null;
        private TermPaging TermPage = null;
        INotifierForWinSvc Notifier;

        public TComThreadMgr(TCtsLogger aLogger): base()
        {
            Logger = aLogger;
            Notifier = new TWindowsNotifier(Logger);
            Logger.Notifier = Notifier;

            SetDefaults();

            TCtsContext fContext = TCtsContext.Instance;
            TCtsDbIni Ini = new TCtsDbIni(fContext.ConfigTableName);
            const string IniSect = "TComThreadMgr";
            int fEmail= Ini.ReadInteger(fContext.ContextKey, IniSect, "Email", 10);
            int fSmtp = Ini.ReadInteger(fContext.ContextKey, IniSect, "SMTP", 10);
            int fSnpp = Ini.ReadInteger(fContext.ContextKey, IniSect, "SNPP", 10);
            int fTapi = Ini.ReadInteger(fContext.ContextKey, IniSect, "Tapi", 10);
            int fTerm = Ini.ReadInteger(fContext.ContextKey, IniSect, "Term", 10);
            string fEMailServer =        Ini.ReadString( fContext.ContextKey, IniSect, "MailServer", "");
            int fEMailPort =             Ini.ReadInteger(fContext.ContextKey, IniSect, "MailServerPort", 0);
            bool fEMailSSL =             Ini.ReadBool(   fContext.ContextKey, IniSect, "MailServerUseSSL", false);
            string fEMailDefaultSender = Ini.ReadString( fContext.ContextKey, IniSect, "Email Default Sender", "");
            string fSmtpDefaultSender =  Ini.ReadString( fContext.ContextKey, IniSect, "Smtp Default Sender", "");
            string fLocalAddress =       Ini.ReadString( fContext.ContextKey, IniSect, "SNPP local address", "");
            bool   fLogAllMessages =     Ini.ReadBool(   fContext.ContextKey, IniSect, "LogAllMessages", true);
            string fPageUser  = Ini.ReadString(fContext.ContextKey, IniSect, "Smtp page User", "");
            string fPagePw    = Ini.ReadString(fContext.ContextKey, IniSect, "Smtp page Password", "");
            string fEmailUser = Ini.ReadString(fContext.ContextKey, IniSect, "Email User", "");
            string fEmailPw   = Ini.ReadString(fContext.ContextKey, IniSect, "Email Password", "");

            SmtpPage = new SmtpPaging(fSmtp, aLogger);
            SmtpPage.IsLogMsgs = fLogAllMessages;
            SmtpPage.DefaultSender = fSmtpDefaultSender;
            SmtpPage.Password = fPagePw;
            SmtpPage.User = fPageUser;
            SmtpPage.Host = fEMailServer;
            SmtpPage.Port = fEMailPort;
            SmtpPage.EnableSsl = fEMailSSL;

            EMail = new EMailPolling(fEmail, aLogger);
            EMail.IsLogMsgs = fLogAllMessages;
            EMail.DefaultSender = fEMailDefaultSender;
            EMail.Password = fEmailPw;
            EMail.User = fEmailUser;
            EMail.Host = fEMailServer;
            EMail.Port = fEMailPort;
            EMail.EnableSsl = fEMailSSL;

            SnppPage = new SnppPaging(fSnpp, aLogger);
            SnppPage.IsLogMsgs = fLogAllMessages;
            SnppPage.LocalAddress = fLocalAddress;

            TapiPage = new TapiPaging(fTapi, aLogger);
            TapiPage.IsLogMsgs = fLogAllMessages;

            TermPage = new TermPaging(fTerm, aLogger);
            TermPage.IsLogMsgs = fLogAllMessages;
        }

        private void SetDefaults()
        {
            List<string> List = new List<string>();
            List.Add("'1iC|UFfF579enhcYtKgWxf', 'ComWsvc', 'logger', 'Loglevel', '4', 'How much data do you want?'");
            List.Add("'32MIGdU29AFQsuKMnQq5Ek', 'ComWsvc', 'TComThreadMgr', 'Email', '10', 'polling period'");
            List.Add("'25Sin3YATC|PxznT9q{MZb', 'ComWsvc', 'TComThreadMgr', 'Email Default Sender', 'Some.One@YourOrg.com', 'All EMails are sent under this name'");
            List.Add("'3NYMPiL{9CAu3|2inMo2FU', 'ComWsvc', 'TComThreadMgr', 'Email page Password', '', 'Password for Email server'");
            List.Add("'2El9uXqhjBZg1uY6V5S8AP', 'ComWsvc', 'TComThreadMgr', 'Email page User', '', 'Username for Email server'");
            List.Add("'2Ix5f7Fuf8W9MsM8g|5ck5', 'ComWsvc', 'TComThreadMgr', 'LogAllMessages', '1', 'Log all messages to TransLog?'");
            List.Add("'2lfl9NS1n1IvLRNQ3lE73N', 'ComWsvc', 'TComThreadMgr', 'MailServer', 'it-mail.YourOrg', 'Name for SMTP email server'");
            List.Add("'3ZtFYmCyv1QBZvaKWRSofj', 'ComWsvc', 'TComThreadMgr', 'MailServerPort', '0', 'SMTP email server port'");
            List.Add("'3sTEtaoe56oukU{Sa57ooF', 'ComWsvc', 'TComThreadMgr', 'MailServerUseSSL', 'false', 'SMTP email server requires SSL?'");
            List.Add("'1S3BailCH45RbnpMWJiN15', 'ComWsvc', 'TComThreadMgr', 'Smtp', '10', 'polling period'");
            List.Add("'0nMbDbjib64wMJWCeRUulM', 'ComWsvc', 'TComThreadMgr', 'Smtp Default Sender', 'Some.One@YourOrg.com', 'All Smtp Pages are sent under this name'");
            List.Add("'1mfgkQyq92HO|jo|n5mANV', 'ComWsvc', 'TComThreadMgr', 'Smtp page Password', '', 'Password for Smtp pages'");
            List.Add("'0O0imqR6r1hPYWHCaPsiY{', 'ComWsvc', 'TComThreadMgr', 'Smtp page User', '', 'Username for Smtp pages'");
            List.Add("'0f|tdAMI58Y98jQVr0uISC', 'ComWsvc', 'TComThreadMgr', 'Snnp', '10', 'polling period'");
            List.Add("'13ciAnjMr9ogtkEN9l2|XC', 'ComWsvc', 'TComThreadMgr', 'Snpp local address', 'xxx.xxx.xx.xx', 'Local Ip address for WinsockPort'");
            List.Add("'0aLSHUOIb2jRVJltHkb1LB', 'ComWsvc', 'TComThreadMgr', 'Tapi', '10', 'polling period'");
            List.Add("'05H3oQpJj6vRlt6XijQxDc', 'ComWsvc', 'TComThreadMgr', 'Term', '10', 'polling period'");

            List.Add("'3A7HVaEvXBsBY|ADy1hy0G', 'ComWsvc', 'TCtsWindowsNotifier',   'EMailPeriod',   '30',      'How often an Email can be sentin minutes.'");
            List.Add("'3pHk|EkaH3cw9f3B6nxfTk', 'ComWsvc', 'TCtsWindowsNotifier',   'Functional',    'false',   'Use notifier'");
            List.Add("'1tEiIr4QPBY86LMwg|PB3Q', 'ComWsvc', 'TCtsWindowsNotifier',   'MaxExceptions', '20',      'Threashold # of errors to invoke an Email'");
            List.Add("'2FNAizxQb6yBj06wUmEIs5', 'ComWsvc', 'TCtsWindowsNotifier',   'TimePeriod',    '30',      'Time period for error count in minutes.'");

          //List.Add("'0qEuT{dsPFu9UgnYM3YwtV', 'ComWsvc', 'TWindowsNotifierEmail', 'LocalHost',     '',        'Local host for mail server'");
            List.Add("'3UKCIRMHj5mxSAkAPV4AKQ', 'ComWsvc', 'TWindowsNotifierEmail', 'Password',      '',        'password for mail server'");
            List.Add("'37q1iyFNH9f88vE5TP2jkr', 'ComWsvc', 'TWindowsNotifierEmail', 'Port',          '25',      'Port for mail server'");
            List.Add("'3cXIuGPen0Af1JrnFF{W|G', 'ComWsvc', 'TWindowsNotifierEmail', 'Server',        'it-mail.YourOrg.com',  'IP for server'");
            List.Add("'2Bu|4l{fD2cvEVUvMFC|bZ', 'ComWsvc', 'TWindowsNotifierEmail', 'User',          '',        'username for mail server'");
            List.Add("'02qgUqu39B{8sdbmzkaNx2', 'ComWsvc', 'TWindowsNotifierEmail', 'EnableSsl',     'false',   'mail server uses SSL'");

            List.Add("'38O8k7bsz7GQbzfQM0DW70', 'ComWsvc', 'TWindowsNotifierEmail', 'SendToError',   'Some.One@YourOrg.com', 'Who the message is to'");
            List.Add("'1vaeJ3IzfESAZwkQ|E2ZCq', 'ComWsvc', 'TWindowsNotifierEmail', 'SenderError',   'Some.One@YourOrg.com', 'Whom the message is from'");
            List.Add("'1TpLZsMTT5hQjNOWGLf4WJ', 'ComWsvc', 'TWindowsNotifierEmail', 'SubjectError',  'MNIUpdateSvc is in distress', 'Subject of message'");
            List.Add("'2X|X|Aw1b7ZuUfNLsPP|0f', 'ComWsvc', 'TWindowsNotifierEmail', 'MessageError',  'MNIUpdateSvc is in distress', 'Body of message'");

            List.Add("'1GK36L0m16jur09Zxupqf3', 'ComWsvc', 'TWindowsNotifierEmail', 'SendToStill',   'Some.One@YourOrg.com', 'Who the message is to'");
            List.Add("'0mKKhca1DAyhvm5hywJG2M', 'ComWsvc', 'TWindowsNotifierEmail', 'SenderStill',   'Some.One@YourOrg.com', 'Who the message is from'");
            List.Add("'1BWUOwrib9lBBfs1iwNVpI', 'ComWsvc', 'TWindowsNotifierEmail', 'SubjectStill',  'MNIUpdateSvc is still running properly', 'Subject of message'");
            List.Add("'10gETxCCb11BQ5uGGX5oYj', 'ComWsvc', 'TWindowsNotifierEmail', 'MessageStill',  'MNIUpdateSvc is still running properly', 'Body of Message'");

            TCtsDbIni Ini = new TCtsDbIni(TCtsContext.Instance.ConfigTableName);
            Ini.BulkInsert(List);
        }

        public void Pause()
        {
            if (EMail != null) { EMail.Pause(); }
            if (SmtpPage != null) { SmtpPage.Pause(); }
            if (SnppPage != null) { SnppPage.Pause(); }
            if (TapiPage != null) { TapiPage.Pause(); }
            if (TermPage != null) { TermPage.Pause(); }
        }

        public void Resume()
        {
            if (EMail != null) { EMail.Resume(); }
            if (SmtpPage != null) { SmtpPage.Resume(); }
            if (SnppPage != null) { SnppPage.Resume(); }
            if (TapiPage != null) { TapiPage.Resume(); }
            if (TermPage != null) { TermPage.Resume(); }
        }

        public void Done()
        {
            if (EMail != null) { EMail.GoAndDie(); }
            if (SmtpPage != null) { SmtpPage.GoAndDie(); }
            if (SnppPage != null) { SnppPage.GoAndDie(); }
            if (TapiPage != null) { TapiPage.GoAndDie(); }
            if (TermPage != null) { TermPage.GoAndDie(); }
        }
    }
}
