﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

using UtilClassLibrary;
using CtsClassLibrary;

namespace ComWSvc
{
    public class SmtpPaging : PagingThread
    {
        public string DefaultSender { get; set; }
        public string User
        { 
            get{ return Setup.UserName;} 
            set{ Setup.UserName = value;}
        }

        public string Password 
        { 
            get{ return Setup.Password;} 
            set{ Setup.Password = value;} 
        }

        public string Host
        {
            get { return Setup.Host; }
            set { Setup.Host = value; }
        }

        public int Port
        {
            get { return Setup.Port; }
            set { Setup.Port = value; }
        }

        public bool EnableSsl
        {
            get { return Setup.EnableSsl; }
            set { Setup.EnableSsl = value; }
        }

        private EMailSetup Setup = new EMailSetup();

        public SmtpPaging(int aPollingPeriod, TCtsLogger aLogger)
            : base(aPollingPeriod, aLogger)
        {
            fLogger.Write("SmtpPaging.Create", TLogLevel.llFlow);
        }

        private bool Post(List<string> List, EMailMessage aMsg)
        {
            fLogger.Write("SmtpPaging.Post", TLogLevel.llFlow);
            try
            {
                aMsg.ToAddr = "";
                foreach(string item in List){ aMsg.ToAddr += item + ", "; }
                aMsg.ToAddr = aMsg.ToAddr.Substring(0, aMsg.ToAddr.Length - 1);
                new TEmail(fLogger, Setup).Send(aMsg);
                fLogger.Write("SmtpPaging.Post Sent to: " + aMsg.ToAddr, TLogLevel.llTransAction);
                return true;
            }
            catch(Exception e)
            {
                fLogger.Write("SmtpPaging.Post SEND ERROR: " + e.Message
                    + " Mail Server:  " + Setup.Host
                    + " Mail Port:    " + Convert.ToString(Setup.Port)
                    + " Local Host:   " + Setup.Host
                    + " Send From:    " + aMsg.FromAddr
                    + " Send To:      " + aMsg.ToAddr
                    + " Message Text: " + aMsg.Message, TLogLevel.llException);
                return false;
            }
        }

        private void PostAndCleanup(List<string> List, string aLastPagedBy, string aLastMessage,
            string aPagerNos, string aCallNos, string aDeletesXml, EMailMessage aMsg)
        {
            TCtsQuery Proc = new TCtsQuery("Mct_Web");
            if (Post(List, aMsg))
            {
                if (IsLogMsgs) { TransLog("CPage", aLastPagedBy, aLastMessage, aPagerNos, aCallNos, aLastPagedBy); }

                fLogger.Write("SMTPPaging.PostAndCleanup "+ TXml.Encode("Delete", aDeletesXml), TLogLevel.llLogic);
                Proc.ParmByName("@DeleteXml", SqlDbType.VarChar, TXml.Encode("Delete", aDeletesXml));
                Proc.ParmByName("@UpDateXml", SqlDbType.VarChar, TXml.Encode("Update", ""));
                Proc.ExecuteProc("dbo.spsc_UpdateDeletePages");
                Proc.Close();
            }
            else
            {
                if (IsLogMsgs) { TransLog("XPage", aLastPagedBy, aLastMessage, aPagerNos, aCallNos, aLastPagedBy); }

                fLogger.Write("SMTPPaging.PostAndCleanup "+ TXml.Encode("Update", aDeletesXml), TLogLevel.llLogic);
                Proc.ParmByName("@DeleteXml", SqlDbType.VarChar, TXml.Encode("Delete", ""));
                Proc.ParmByName("@UpDateXml", SqlDbType.VarChar, TXml.Encode("Update", aDeletesXml));
                Proc.ExecuteProc("dbo.spsc_UpdateDeletePages");
                Proc.Close();
            }
        }

        protected override void Event()
        {
            try
            {
                fLogger.Write("SmtpPaging.Event", TLogLevel.llFlow);
                EMailMessage Msg = new EMailMessage();
                string DeletesXml = "";
                string PagerNos = "";
                string CallNos = "";
                List<string> List = new List<string>();
                TCtsQuery Query = new TCtsQuery("Mct_Web");
                SqlDataReader Reader = Query.ExecuteProc("dbo.spsc_GetSMTPPages");
                fLogger.Write("SmtpPaging.Event 1", TLogLevel.llIteration);
                if (Reader.Read())
                {
                    fLogger.Write("SmtpPaging.Event 1.1", TLogLevel.llIteration);
                    string LastPagedBy = (string)Reader["PagedBy"];
                    string LastMessage = (string)Reader["MESSAGE"];
                    Msg.Subject = "SmartCOP Page";
                    Msg.Message = (string)Reader["MESSAGE"];
                    Msg.FromAddr = DefaultSender;
                    Msg.ToAddr = "";
                    List.Add((string)Reader["PAGER"] + "@" + (string)Reader["Mail_Domain"]);
                    DeletesXml += TXml.Encode("row", TXml.Encode("Uniquekey", (string)Reader["UNIQUEKEY"]));
                    PagerNos += (string)Reader["PagerNo"];
                    CallNos += (string)Reader["CallNo"];
                    while (Reader.Read())
                    {
                        fLogger.Write("SmtpPaging.Event 1.1.1", TLogLevel.llIteration);
                        //control break on PagedBy and Message
                        if ((LastPagedBy != (string)Reader["PagedBy"])
                          | (LastMessage != (string)Reader["MESSAGE"]))
                        {
                            PostAndCleanup(List, LastPagedBy, LastMessage, PagerNos, CallNos, DeletesXml, Msg);
                            LastPagedBy = (string)Reader["PagedBy"];
                            LastMessage = (string)Reader["MESSAGE"];
                            List.Clear();
                            DeletesXml = "";
                            PagerNos = "";
                            CallNos = "";
                            Msg.Subject = "SmartCOP Page";
                            Msg.Message = (string)Reader["MESSAGE"];
                            Msg.FromAddr = DefaultSender;
                            Msg.ToAddr = "";
                        }
                        fLogger.Write("SmtpPaging.Event 1.1.2", TLogLevel.llIteration);
                        List.Add((string)Reader["PAGER"] + "@" + (string)Reader["Mail_Domain"]);
                        DeletesXml += TXml.Encode("row", TXml.Encode("Uniquekey", (string)Reader["UNIQUEKEY"]));
                        PagerNos += (string)Reader["PagerNo"];
                        CallNos += (string)Reader["CallNo"];
                        fLogger.Write("SmtpPaging.Event 1.1.3", TLogLevel.llIteration);
                    }
                    fLogger.Write("SmtpPaging.Event 1.2", TLogLevel.llIteration);
                    PostAndCleanup(List, LastPagedBy, LastMessage, PagerNos, CallNos, DeletesXml, Msg);
                    fLogger.Write("SmtpPaging.Event 1.3", TLogLevel.llIteration);
                }
                fLogger.Write("SmtpPaging.Event 2", TLogLevel.llIteration);
            }
            catch(Exception e)
            {
                fLogger.Write("SmtpPaging.Event " + e.Message, TLogLevel.llException);
            }
        }
    }
}
