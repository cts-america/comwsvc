﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections;

namespace CtsClassLibrary
{
    public class TIni
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
                 string key, string def, StringBuilder retVal,
            int size,string filePath);

        [DllImport("Kernel32.dll", EntryPoint = "GetPrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetPrivateProfileString(string lpAppName,
                                                         string lpKeyName,
                                                         string lpDefault,
                                                         [In, Out] char[] lpReturnString,
                                                         int nSize,
                                                         string lpFilename);
 
        public TIni(string INIPath)
        {
            path = INIPath;
        }

        private void WriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }
        
        private bool ReadValue(string Section, string Key, out string Value)
        {
            Value = "";
            StringBuilder temp = new StringBuilder(255);
            int Ii = GetPrivateProfileString(Section, Key, null, temp, 255, this.path);
            if (Ii > 0)
            {
                Value = temp.ToString();
            }
            return (Ii > 0);
        }

        public string ReadString(string Section, string Key, string Def)
        {
            string Value;
            if( ReadValue(Section, Key, out Value))
            {
                return Value;
            }
            else
            {
                return Def;
            }
        }
        //todo add reads and writes

        public void ReadSection(string Section, TStrings Strings)
        {
            string[] List = GetPrivateProfileString(Section, null, null, this.path);
            foreach (string item in List)
            {
                TNameValue nv = new TNameValue();
                nv.Name = item;
                Strings.Add(nv);
            }
        }
        public void ReadSections(TStrings Strings)
        {
            string[] List = GetPrivateProfileString(null, null, null, this.path);
            foreach (string item in List)
            {
                TNameValue nv = new TNameValue();
                nv.Name = item;
                Strings.Add(nv);
            }
        }

        public void ReadSectionValues(string Section, TStrings Strings)
        {
            string temp;
            string[] List = GetPrivateProfileString(Section, null, null, this.path);
            foreach (string item in List)
            {
                TNameValue nv = new TNameValue();
                nv.Name = item;
                ReadValue(Section, item, out temp);
                nv.Value = temp;
                Strings.Add(nv);
            }
        }

        public void EraseSection(string Section)
        {
          WritePrivateProfileString(Section, null, null, this.path);
        }

        public Boolean SectionExists(string Section)
        {
            TStrings Strings = new TStrings();
            ReadSection(Section, Strings);
            return Strings.Count > 0;
        }

        private string[] GetPrivateProfileString(string Section, string Name, string defaultValue, string inifile)
        {
            int _readBufferSize = 16384;
            char[] _readBuffer = new char[_readBufferSize];
            int sizeRead = GetPrivateProfileString(Section, Name, defaultValue, _readBuffer, _readBufferSize, inifile);

            //  If neither lpAppName nor lpKeyName is NULL and the supplied destination buffer is too small to hold the requested string, the string is truncated and followed by a null character, and the return value is equal to nSize minus one.
            //  If either lpAppName or lpKeyName is NULL and the supplied destination buffer is too small to hold all the strings, the last string is truncated and followed by two null characters. In this case, the return value is equal to nSize minus two.
            int sizeReadOffset = (Section != null && Name != null) ? 1 : 2;
            if (sizeRead == _readBufferSize - sizeReadOffset)
                throw new Exception("Buffer too small");

            // the buffer returned from GetPrivateProfileString will be null terminated C-strings followed by a double null at the end - so split the strings on the nulls
            char[] sep = new char[1];
            sep[0] = '\0';

            string s = new string(_readBuffer, 0, sizeRead);

            string[] result = s.Split(sep, StringSplitOptions.RemoveEmptyEntries);

            return result;
        }
    }
}
