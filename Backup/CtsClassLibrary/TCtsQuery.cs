﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace CtsClassLibrary
{
    public class TCtsBaseSql
    {
        static public string EncodeTo64(string toEncode)
        {
          byte[] toEncodeAsBytes 
                = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);

          string returnValue 
                = System.Convert.ToBase64String(toEncodeAsBytes);

          return returnValue;
        }
        
        static public string DecodeFrom64(string encodedData)
        {
          byte[] encodedDataAsBytes 
              = System.Convert.FromBase64String(encodedData);

          string returnValue = 
             System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

          return returnValue;
        }

        public bool Usable(Object Item)
        {
            if (Item == null) { return false; }
            if (Item.ToString() == "") { return false; }
            return true;
        }

        public DateTime GetDate(Object Item)
        {
            if (this.Usable(Item)) 
            { 
                return DateTime.Parse(Item.ToString()); 
            }
            else
            { 
                return DateTime.MinValue; 
            } 
        }

        public string GetDataAsXml(ref SqlDataReader aReader, string aRowName)
        {
            string result = "";
            while (aReader.Read())
            {
                result += TXml.Encode(aRowName, GetRowAsXml(ref aReader));
            }
            return result;
        }

        public string GetRowAsXml(ref SqlDataReader aReader )
        {
            string result = "";
            for (int Ii = 0; Ii < aReader.FieldCount; Ii++ )
            {
                //todo track this down:
                System.Type fType = aReader.GetFieldType(Ii);
                if( fType.Equals(typeof(System.Byte[])))
                {
                    result += EncodeTo64( TXml.Encode(aReader.GetName(Ii), aReader[Ii].ToString()));
                }
                else
                {
                    result += TXml.Encode(aReader.GetName(Ii), aReader[Ii].ToString());
                }
            }
            return result;
        }
    }

    public class TCtsQuery: TCtsBaseSql
    {
        private SqlConnection myConnection;
        private List<SqlParameter> Parms = new List<SqlParameter>();
        
        public TCtsQuery(string DBName) : this(TCtsContext.Instance.GetConnStr(DBName), false)
        {          
        }
        
        public TCtsQuery(string ConnectString, bool aBool)
        {
            myConnection = new SqlConnection(ConnectString);
        }

        public int ExecuteNonQuery(string Statement)
        {
            try
            {
                SqlCommand myCommand = new SqlCommand();
                myCommand.Connection = myConnection;
                if (myConnection.State != ConnectionState.Open) { myConnection.Open(); }
                myCommand.CommandText = Statement;
                foreach (SqlParameter item in Parms)
                {
                    myCommand.Parameters.Add(item);
                }
                int result = myCommand.ExecuteNonQuery();
                return result;
            }
            finally
            {
                Parms.Clear();
                myConnection.Close();
            }
        }
        
        public SqlDataReader ExecuteProc(string StoredProcName)
        {
            try
            {
                SqlCommand myCommand = new SqlCommand();
                myCommand.Connection = myConnection;
                if (myConnection.State != ConnectionState.Open) { myConnection.Open(); }
                myCommand.CommandText = StoredProcName;
                myCommand.CommandType = CommandType.StoredProcedure;
                foreach (SqlParameter item in Parms)
                {
                    myCommand.Parameters.Add(item);
                }
                SqlDataReader myReader = myCommand.ExecuteReader();
                return myReader;
            }
            finally
            {
                Parms.Clear();
            }
        }
        
        public SqlDataReader ExecuteQuery(string Statement)
        {
            try
            {
                SqlCommand myCommand = new SqlCommand();
                myCommand.Connection = myConnection;
                if (myConnection.State != ConnectionState.Open) { myConnection.Open(); }
                myCommand.CommandText = Statement;
                foreach (SqlParameter item in Parms)
                {
                    myCommand.Parameters.Add(item);
                }
                SqlDataReader myReader = myCommand.ExecuteReader();
                return myReader;
            }
            finally
            {
                Parms.Clear();
            }
            /*
            while(myReader.Read())
            {
            int i = (int)myReader["Column1"];
            Console.WriteLine(myReader["Column1"].ToString());
            Console.WriteLine(myReader["Column2"].ToString());
            if (rdr != null){rdr.Close();}
            }
            */
        }

        public void Close()
        {
            if (myConnection.State == ConnectionState.Open)
            {
                myConnection.Close();
            }            
        }

        public void ParmByName(string aName, System.Data.SqlDbType aType, Object aValue)
        {
            SqlParameter myParam = new SqlParameter(aName, aType);
            myParam.Value = aValue;
            Parms.Add(myParam);
        }
    }

    public class TCtsProc: TCtsBaseSql
    {
        SqlCommand myCommand = new SqlCommand();
        private SqlConnection myConnection;
        
        public TCtsProc(string DBName, string StoredProcName)
        {
            myConnection = new SqlConnection(TCtsContext.Instance.GetConnStr(DBName));
            myCommand.Connection = myConnection;
            if (myConnection.State != ConnectionState.Open) { myConnection.Open(); }
            myCommand.CommandText = StoredProcName;
            myCommand.CommandType = CommandType.StoredProcedure;
        }

        public SqlDataReader ExecuteProc()
        {
            SqlDataReader myReader = myCommand.ExecuteReader();
            return myReader;    
        }

        public Object ExecuteScalar()
        {
            Object myObj = myCommand.ExecuteScalar();
            return myObj;    
        }

        public XmlReader ExecuteXmlReader()
        {
            XmlReader myReader = myCommand.ExecuteXmlReader();
            return myReader;    
        }

        public void Close()
        {
            if (myConnection.State == ConnectionState.Open)
            {
                myConnection.Close();
            }
            myCommand.Parameters.Clear();
        }

        public void SetParmByName(string aName, System.Data.SqlDbType aType, Object aValue)
        {
            SetParmByName(aName, aType, aValue, ParameterDirection.Input);
        }

        public void SetParmByName(string aName, System.Data.SqlDbType aType, Object aValue, ParameterDirection aDir)
        {
            SqlParameter myParam = new SqlParameter(aName, aType);
            myParam.Value = aValue;
            myParam.Direction = aDir;
            myCommand.Parameters.Add(myParam);
        }

        public SqlParameter GetParmByName(string aName)
        {
            return myCommand.Parameters[aName];
        }
    }
}
