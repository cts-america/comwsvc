﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CtsClassLibrary
{
    public interface INotifierForWinSvc
    {
        void Execute(bool aException);
    }

    public enum TLogHelper { lhDebugStrings, lhFile };
    public enum TLogLevel  { llException, llLogic, llTransAction, llDebug, llFlow, llIteration};

    public class TCtsLogger
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern void OutputDebugStringA(string lpOutputString);

        private string[] C_LogLevel = new string[6]{"Error", "Logic", "TransAction", "Debug", "Flow", "Iteration"};

        private delegate void dLogMessage(string aMsg, TLogLevel aLogLevel);
        private dLogMessage LogMessage;
        private TLogLevel CurrentLogLevel = TLogLevel.llFlow;
        private string Filename = "";
        static object locker = new object();
        private INotifierForWinSvc fNotifier = null;
        public INotifierForWinSvc Notifier { get { return fNotifier; } set { fNotifier = value; } }

        private void LogMessageToFile(string aMsg, TLogLevel aLogLevel)
        {
            using (StreamWriter SW = File.AppendText(Filename))
            {
                SW.WriteLine(aMsg);
                //SW.Close();
            }
        }

        private void LogMessageToDebugStrings(string aMsg, TLogLevel aLogLevel)
        {
            OutputDebugStringA(aMsg);
        }

        public TCtsLogger(TLogHelper helper)
        {
            switch (helper)
            {
                case TLogHelper.lhDebugStrings:
                    LogMessage = LogMessageToDebugStrings;
                    break;
                case TLogHelper.lhFile:
                    LogMessage = LogMessageToFile;
                    break;
            }
            Setdata();
        }

        public void Setdata()
        {
            try
            {
                Filename = TCtsContext.Instance.GetAppPath() + TCtsContext.Instance.GetRootname() + ".log";

                Write("ConfigTableName: " + TCtsContext.Instance.ConfigTableName, TLogLevel.llDebug);
                Write("ContextKey:      " + TCtsContext.Instance.ContextKey, TLogLevel.llDebug);
              //Write("GetConnStr:      " + TCtsContext.Instance.GetConnStr(""), TLogLevel.llDebug);
                Write("GetAppPath:      " + TCtsContext.Instance.GetAppPath(), TLogLevel.llDebug);
                Write("GetRootname:     " + TCtsContext.Instance.GetRootname(), TLogLevel.llDebug);

                TCtsDbIni Ini = new TCtsDbIni(TCtsContext.Instance.ConfigTableName);
                Filename = Ini.ReadString(TCtsContext.Instance.ContextKey, "Logger", "Filename", Filename);
                Write("Log filename:    " + Filename, TLogLevel.llDebug);
                CurrentLogLevel = (TLogLevel)Ini.ReadInteger(TCtsContext.Instance.ContextKey, "Logger", "Loglevel", (int)TLogLevel.llFlow);
            }
            catch (Exception e)
            {
                Write("TCtsLogger.Setdata " + e.Message, TLogLevel.llException);
            }
        }

        public void Write(string aMsg, TLogLevel aLogLevel)
        {
            if (Notifier != null)
            {
                Notifier.Execute(aLogLevel == TLogLevel.llException);
            }

            if (aLogLevel > CurrentLogLevel) { return; }

            string fMessage = EncodeMsg(aMsg, aLogLevel);
            lock (locker)
            {
                LogMessage(fMessage, aLogLevel);
            }
        }

        protected string EncodeMsg(string aMsg, TLogLevel aLogLevel)
        {
            string Temp = "";
            Temp = Temp + TXml.Encode("Application", TCtsContext.Instance.GetRootname());
            Temp = Temp + TXml.Encode("DateTime", System.DateTime.Now.ToString());
            Temp = Temp + TXml.Encode("Level", C_LogLevel[(int)aLogLevel]);
            Temp = Temp + TXml.Encode("Msg", aMsg);
            return TXml.Encode("LogMsg", Temp);
        }
    }
}
