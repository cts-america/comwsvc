﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

namespace CtsClassLibrary
{
    public class TCtsDbIni
    {
        //select top 1 ScAdmin.dbo.FNSC_GetUniqueId(newID())
        string C_Select = "SELECT UserKey,"
                  + " Section,"
                  + " Name,"
                  + " Value"
                  + " FROM :TableName"
                  + " Where upper(UserKey) = upper(@UserKey)"
                  + "   and upper(Section) = upper(@Section)"
                  + "   and upper(Name) = upper(@Name)";
        string C_Insert = "Insert into :TableName"
                  + " ( Uniquekey, UserKey,  Section,  Name,  Value) Values "
                  + " ( ScAdmin.dbo.FNSC_GetUniqueId(newID()), @UserKey, @Section, @Name, @Value)";
        string C_Update = "Update :TableName "
                  + " Set Value = @Value"
                  + " Where upper(UserKey) = upper(@UserKey)"
                  + "   and upper(Section) = upper(@Section)"
                  + "   and upper(Name) = upper(@Name)";
        private TCtsQuery Query = null;

        string fTableName;
        const string C_Table = ":TableName";

        public TCtsDbIni(string aTableName)
        {
            Query = new TCtsQuery("Mct_Switch");
            fTableName = aTableName;
            C_Select = C_Select.Replace(C_Table, fTableName);
            C_Insert = C_Insert.Replace(C_Table, fTableName);
            C_Update = C_Update.Replace(C_Table, fTableName);

        }

        public void DeleteKey(string aUserKey, string aSect, string aName)
        {
            string C_DeleteKey = "Delete from :TableName"
             + " Where upper(UserKey) = upper(@UserKey)"
             + "   and upper(Section) = upper(@Section)"
             + "   and upper(Name) = upper(@Name)";

            C_DeleteKey = C_DeleteKey.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
            Query.ParmByName("@Name",    SqlDbType.VarChar, aName);
            Query.ExecuteNonQuery(C_DeleteKey);
        }

        public void EraseSection(string aUserKey, string aSect)
        {
            string C_EraseSection = "Delete from :TableName"
             + " Where upper(UserKey) = upper(@UserKey)"
             + "   and upper(Section) = upper(@Section)";

            C_EraseSection = C_EraseSection.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
            Query.ExecuteNonQuery(C_EraseSection);

        }

        public void EraseUser(string aUserKey)
        {
            string   C_EraseUser = "Delete from :TableName"
                + " Where upper(UserKey) = upper(@UserKey)";

            C_EraseUser = C_EraseUser.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ExecuteNonQuery(C_EraseUser);
        }

        private Boolean ReadValue(string aUserKey, string aSect, string aName, out string aValue)
        {
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
            Query.ParmByName("@Name", SqlDbType.VarChar, aName);
            SqlDataReader myReader = Query.ExecuteQuery(C_Select);
            if (!myReader.Read())
            {
                aValue = "";
                Query.Close();
                return false;
            }
            else
            {
                aValue = (string)myReader["Value"];
                Query.Close();
                return true;
            }
        }

        public Boolean ReadBool(string aUserKey, string aSect, string aName, Boolean aDefault)
        {
            string Temp;
            if (ReadValue(aUserKey, aSect, aName, out Temp))
            {
                try
                {
                    Boolean Value = Boolean.Parse(Temp);
                    return Value;
                }
                catch
                {   
                    return aDefault; 
                }
            }
            else{   return aDefault; }
        }

        public DateTime ReadDateTime(string aUserKey, string aSect, string aName, DateTime aDefault)
        {
            string Temp;
            if (ReadValue(aUserKey, aSect, aName, out Temp))
            {
                try
                {
                    DateTime Value = DateTime.Parse(Temp);
                    return Value;
                }
                catch
                {
                    return aDefault;
                }
            }
            else { return aDefault; }
        }

        public float ReadFloat(string aUserKey, string aSect, string aName, float aDefault)
        {
            string Temp;
            if (ReadValue(aUserKey, aSect, aName, out Temp))
            {
                try
                {
                    float Value = float.Parse(Temp);
                    return Value;
                }
                catch
                {
                    return aDefault;
                }
            }
            else { return aDefault; }
        }

        public int ReadInteger(string aUserKey, string aSect, string aName, int aDefault)
        {
            string Temp;
            if (ReadValue(aUserKey, aSect, aName, out Temp))
            {
                try
                {
                    int Value = int.Parse(Temp);
                    return Value;
                }
                catch
                {
                    return aDefault;
                }
            }
            else { return aDefault; }
        }

        public Boolean SectionExists(string aUserKey, string aSect)
        {
            string C_Select = "SELECT UserKey,"
                + " Section"
                + " FROM :TableName"
                + " Where upper(UserKey) = upper(@UserKey)"
                + "   and upper(Section) = upper(@Section)";

            C_Select = C_Select.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
            SqlDataReader myReader = Query.ExecuteQuery(C_Select);
            bool Temp = myReader.Read();
            Query.Close();
            return Temp;
        }

        public Boolean ValueExists(string aUserKey, string aSect, string aName)
        {
            string C_Select = "SELECT UserKey,"
                + " Section"
                + " FROM :TableName"
                + " Where upper(UserKey) = upper(@UserKey)"
                + "   and upper(Section) = upper(@Section)"
                + "   and upper(Name) = upper(@Name)";

            C_Select = C_Select.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
            Query.ParmByName("@Name",    SqlDbType.VarChar, aName);
            SqlDataReader myReader = Query.ExecuteQuery(C_Select);
            bool Temp = myReader.Read();
            Query.Close();
            return Temp;
        }

        public void ReadSectionValues(string aUserKey, string aSect, TStrings aStrings)
        {
            string C_Select = "SELECT UserKey,"
                + " Section"
                + " FROM :TableName"
                + " Where upper(UserKey) = upper(@UserKey)"
                + "   and upper(Section) = upper(@Section)";

            C_Select = C_Select.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
            SqlDataReader myReader = Query.ExecuteQuery(C_Select);
            while (myReader.Read())
            {
                TNameValue nv = new TNameValue();
                nv.Name =  (string)myReader["Name"];
                nv.Value = (string)myReader["Value"];
                aStrings.Add(nv);
            }
            Query.Close();
        }

        public void ReadSections(string aUserKey, TStrings aStrings)
        {
            string C_Select = "SELECT distinct UserKey,"
                + " Section"
                + " FROM :TableName"
                + " Where upper(UserKey) = upper(@UserKey)";

            C_Select = C_Select.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            SqlDataReader myReader = Query.ExecuteQuery(C_Select);
            while (myReader.Read())
            {
                TNameValue nv = new TNameValue();
                nv.Name =  (string)myReader["Name"];
                aStrings.Add(nv);
            }
            Query.Close();
        }

        public void ReadSections(string aUserKey, List<string> aStrings)
        {
            string C_Select = "SELECT distinct UserKey,"
                + " Section"
                + " FROM :TableName"
                + " Where upper(UserKey) = upper(@UserKey)";

            C_Select = C_Select.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            SqlDataReader myReader = Query.ExecuteQuery(C_Select);
            while (myReader.Read())
            {
                aStrings.Add((string)myReader["Name"]);
            }
            Query.Close();
        }

        public void ReadSection(string aUserKey, string aSect, TStrings aStrings)
        {
            string C_Select = "SELECT UserKey,"
                + " Section,"
                + " Name,"
                + " Value"
                + " FROM :TableName"
                + " Where upper(UserKey) = upper(@UserKey)"
                + "   and upper(Section) = upper(@Section)";

            C_Select = C_Select.Replace(C_Table, fTableName);
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
            SqlDataReader myReader = Query.ExecuteQuery(C_Select);
            while (myReader.Read())
            {
                TNameValue nv = new TNameValue();
                nv.Name =  (string)myReader["Name"];
                nv.Value = (string)myReader["Value"];
                aStrings.Add(nv);
            }
            Query.Close();
        }

        public string ReadString(string aUserKey, string aSect, string aName, string aDefault)
        {
            string Temp;
            if (ReadValue(aUserKey, aSect, aName, out Temp))
            {
                return Temp;
            }
            else { return aDefault; }
        }

        public void WriteString(string aUserKey, string aSect, string aName, string aValue)
        {
            Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
            Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
            Query.ParmByName("@Name", SqlDbType.VarChar, aName);
            Query.ParmByName("@Value", SqlDbType.VarChar, aValue);
            if (Query.ExecuteNonQuery(C_Update) == 0)
            {
                Query.ParmByName("@UserKey", SqlDbType.VarChar, aUserKey);
                Query.ParmByName("@Section", SqlDbType.VarChar, aSect);
                Query.ParmByName("@Name", SqlDbType.VarChar, aName);
                Query.ParmByName("@Value", SqlDbType.VarChar, aValue);
                Query.ExecuteNonQuery(C_Insert);
            }
        }

        public void WriteBool(string aUserKey, string aSect, string aName, Boolean aValue)
        {
            WriteString(aUserKey, aSect, aName, aValue.ToString());
        }

        public void WriteDateTime(string aUserKey, string aSect, string aName, DateTime aValue)
        {
            WriteString(aUserKey, aSect, aName, aValue.ToString());
        }

        public void WriteFloat(string aUserKey, string aSect, string aName, float aValue)
        {
            WriteString(aUserKey, aSect, aName, aValue.ToString());
        }

        public void WriteInteger(string aUserKey, string aSect, string aName, int aValue)
        {
            WriteString(aUserKey, aSect, aName, aValue.ToString());
        }

        public void BulkInsert(List<string> Strings)
        {
//Select top 10 ScAdmin.dbo.FNSC_GetUniqueId(newID()) from [MCT_Switch].[dbo].[Config]
            string C_Insert = "Insert into :TableName"
                            + " (Uniquekey, UserKey, Section, Name, Value, Description)"
                            + "  Select :Select";
            TCtsQuery Query = new TCtsQuery("");
            C_Insert = C_Insert.Replace(C_Table, TCtsContext.Instance.ConfigTableName);
            foreach(string item in Strings)
            {
                try
                {
                    Query.ExecuteNonQuery(C_Insert.Replace(":Select", item));
                }
                catch{ } // eat me!
            }
        }
    }
}
