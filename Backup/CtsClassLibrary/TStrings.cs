﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CtsClassLibrary
{
    public class TNameValue
    {
        public string Name{ get; set; }

        public string Value{ get; set; }
    }

    public class TStrings : List<TNameValue>
    {

        public string Name(int index)
        {
            return this[index].Name;
        }

        public string Value(int index)
        {
            return this[index].Value;
        }

        public string Value(string aName)
        {
            int Ii = this.FindIndex(delegate(TNameValue item){ return aName == item.Name;});
            if (Ii == -1)
            { return ""; }
            else
            { return this[Ii].Value; }
        }
    }
}
