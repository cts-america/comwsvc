﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CtsClassLibrary
{
    public class TXml
    {
        public static string Encode(string aTag, string aValue)
        {
            return Encode(aTag, aValue, "");
        }

        public static string Encode(string aTag, string aValue, string aAttr)
        {
            string Temp = "";
            if (aAttr != "") { Temp = " " + aAttr; }
            return "<"+ aTag + Temp +">"+ aValue +"</"+ aTag +">";
        }

    }
}
