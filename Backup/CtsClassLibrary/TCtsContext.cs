﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Windows.Forms;

namespace CtsClassLibrary
{
    public enum TAppType { atDefault, atWebSvc, atWindowsSvc, atUseEnvVar };

    public sealed class TCtsContext
    {
        const string C_DefaultConnStr = "Data Source=EN-DEV-MO\\MAINLINE;Integrated Security=True";
        const string C_DefaultConfigTableName = "MCT_Switch.dbo.Config";
        const string C_DefaultContextName = "Mobile Forms";
        const string C_ConfigTableName = "ConfigTableName";
        const string C_ConnStr = "ConnStr";
        const string C_ContextName = "ContextKey";
        const string C_ConnStrSectionName = "DBConnStr";

        private TAppType AppType = TAppType.atDefault;
        private static volatile TCtsContext instance;
        private static object syncRoot = new Object();
        private TStrings FConnStrList = new TStrings();
        private string FRootName = "";
        private string FAppPath = "";
        private string FConnstr = "";

        public  string ConfigTableName  { get; set; }
        public  string ContextKey  { get; set; }

        private TCtsContext() 
        {
            if (File.Exists(GetAppPath() + GetRootname() + ".ini"))
            {
                this.SetDataFromFile(this.GetRootname(), GetAppPath() + GetRootname() + ".ini");
            }
            else
            {
                if (File.Exists(this.GetAppPath() + "SmartCop.ini"))
                {
                    this.SetDataFromFile(this.GetRootname(), this.GetAppPath() + "SmartCop.ini");
                }
                else
                {
                    UseDefaults();
                }
            }
        }

        public static TCtsContext Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new TCtsContext();
                    }
                }
                return instance;
            }
        }

        public TCtsContext (TAppType aAppType, string aRootName)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                    {
                        FRootName = aRootName;
                        AppType = aAppType;
                        if (File.Exists(GetAppPath() + GetRootname() + ".ini"))
                        {
                            this.SetDataFromFile(this.GetRootname(), GetAppPath() + GetRootname() + ".ini");
                        }
                        else
                        {
                            if (File.Exists(this.GetAppPath() + "SmartCop.ini"))
                            {
                                this.SetDataFromFile(this.GetRootname(), this.GetAppPath() + "SmartCop.ini");
                            }
                            else
                            {
                                UseDefaults();
                            }
                        }
                        instance = this;
                    }
                }
            }
        }

        private string GetPathFromEnvironment()
        {
            string result = System.Environment.GetEnvironmentVariable("SmartCopInc");
            if (result == "")
            {
                throw new Exception("Environmental variable \'SmartCopInc\' does not exist");
            }
            if (result.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()))
            {
                return result;
            }
            else
            {
                return (result + System.IO.Path.DirectorySeparatorChar);
            }
        }

        private void GetRootAndPath()
        {
            //todo this is not ness correct for all cases
            switch (AppType)
            {
                case TAppType.atDefault: //untested for default
                case TAppType.atWindowsSvc:  //this will work for a windows service
                    string fFilename = Application.ExecutablePath; 
                    FAppPath = Path.GetDirectoryName(fFilename);
                    if ( FRootName == "")
                    {
                        FRootName = Path.GetFileNameWithoutExtension(Application.ExecutablePath);
                    }                    
                    break;
                case TAppType.atWebSvc: //this will work for a web service in testing
                    FAppPath = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                    //FRootName = must be passed in 
                    break;
                case TAppType.atUseEnvVar: //untested
                    FAppPath = GetPathFromEnvironment();
                    if ( FRootName == "")
                    {
                        FRootName = Path.GetFileNameWithoutExtension(Application.ExecutablePath);
                    }
                    break;
            }
            if ( FAppPath.Substring(FAppPath.Length-1, 1) != "\\") {FAppPath = FAppPath+ "\\"; }
        }

        public  string GetRootname()
        {
            if (FRootName == "" )
            {
                GetRootAndPath();
            }
            return FRootName;
        }

        public string GetAppPath()
        {
            if (FAppPath == "" )
            {
                GetRootAndPath();
            }
            return FAppPath;  
        }

        private void SetDataFromFile(string aSectionName, string aFilename)
        {
            TIni ini = new TIni(aFilename);
            string fSectionName;
            if (ini.SectionExists(aSectionName))
            {
                fSectionName = aSectionName;
            }
            else
            {
                fSectionName = "SmartCop";
            }

            ConfigTableName = ini.ReadString(fSectionName, C_ConfigTableName, C_DefaultConfigTableName);
            FConnstr        = ini.ReadString(fSectionName, C_ConnStr,         C_DefaultConnStr);
            ContextKey      = ini.ReadString(fSectionName, C_ContextName,     C_DefaultContextName);

            if (ini.SectionExists(aSectionName+".DB"))
            {
                ini.ReadSectionValues(aSectionName+".DB", FConnStrList);
            }
            else
            {
                if (ini.SectionExists("SmartCop.DB"))
                {
                    ini.ReadSectionValues(aSectionName + ".DB", FConnStrList);
                }
                else
                {
                    ini.ReadSectionValues(C_ConnStrSectionName, FConnStrList);
                }
            }
        }

        public string GetConnStr(string aDbName)
        {
            const string C_Param = "Initial Catalog=:DB";
            if (aDbName == "")
            {
                return FConnstr;
            }

            string result = FConnStrList.Value(aDbName);
            if (result == "")
            {
                if (FConnstr.Substring(FConnstr.Length -1, 1) == ";")
                {
                    return FConnstr + C_Param.Replace(":DB", aDbName);
                }
                else
                {
                    return FConnstr + ";" + C_Param.Replace(":DB", aDbName);
                }
            }
            else
            {
                return result;
            }
        }

        private void UseDefaults()
        {
            ConfigTableName = C_DefaultConfigTableName;
            FConnstr        = C_DefaultConnStr;
            ContextKey      = C_DefaultContextName;
        }
    }
}
