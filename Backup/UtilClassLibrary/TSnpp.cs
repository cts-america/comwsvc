﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CtsClassLibrary;

namespace UtilClassLibrary
{
    public class SnppSetup
    {
        public SnppSetup() { }

        private int fPort = 0;
        public int Port { get { return fPort; } set { fPort = value; } }
        private string fLocalAddress = "";
        public string LocalAddress { get { return fLocalAddress; } set { fLocalAddress = value; } }
    }

    public class SnppMessage
    {
        public SnppMessage() { }

        private string fPagerNo = "";
        public string PagerNo { get { return fPagerNo; } set { fPagerNo = value; } }
        private string fAddress = "";
        public string Address { get { return fAddress; } set { fAddress = value; } }
        private string fPort = "";
        public string Port { get { return fPort; } set { fPort = value; } }
        private string fMessage = "";
        public string Message { get { return fMessage; } set { fMessage = value; } }
    }

    public class TSnpp
    {
        private SnppSetup Setup;

        public TCtsLogger Logger = null;

        public TSnpp(TCtsLogger aLogger, SnppSetup aSetup)
        {
            Logger = aLogger;
            Setup = aSetup;
        }
        /*
         *  2xx - Successful, continue
         *  3xx - Begin DATA input (see "DATA" command)
         *  4xx - Failed with connection terminated
         *  5xx - Failed, but continue session SNPP version 3 (two-way) adds the following categories:
         *  7xx - UNsuccessful two-way specific transaction, but continue session
         *  8xx - Successful two-way specific transaction, continue
         *  9xx - Successful QUEUED two-way transaction, continue
         */
        public void Send(SnppMessage SM)
        {
            const string SendPagerCommand = "PAGE ";
            const string SendMessageCommand = "MESS ";
            const string FinishSendCommand = "SEND ";
            const string TransactionCompleteCommand = "QUIT ";
            try
            {
				Logger.Write("TSnpp.Send Before creating PagingClient", TLogLevel.llFlow);
				System.Net.Sockets.TcpClient PagingClient = new System.Net.Sockets.TcpClient(SM.Address, Int32.Parse(SM.Port));
				Logger.Write("TSnpp.Send Before creating PagingStream", TLogLevel.llFlow);
				System.Net.Sockets.NetworkStream PagingStream = PagingClient.GetStream();
				Logger.Write("TSnpp.Send Checking response", TLogLevel.llFlow);
				CheckResponse(PagingStream);
				Logger.Write("TSnpp.Send Sending " + SendPagerCommand + SM.PagerNo, TLogLevel.llFlow);
				SendMessage(PagingStream, SendPagerCommand + SM.PagerNo + System.Environment.NewLine);
				Logger.Write("TSnpp.Send Sending " + SendMessageCommand, TLogLevel.llFlow);
				SendMessage(PagingStream, SendMessageCommand + SM.Message + System.Environment.NewLine);
				Logger.Write("TSnpp.Send Sending " + FinishSendCommand, TLogLevel.llFlow);
				SendMessage(PagingStream, FinishSendCommand + System.Environment.NewLine);
				Logger.Write("TSnpp.Send Sending " + TransactionCompleteCommand, TLogLevel.llFlow);
				SendMessage(PagingStream, TransactionCompleteCommand + System.Environment.NewLine);
            }
            catch (Exception E)
            {
                if (Logger != null)
                {   Logger.Write("TSnpp.Send " + E.Message, TLogLevel.llException); }
                else
                {   throw new Exception("TSnpp.Send " + E.Message); }
            }
        }

        private string ReadMessage(System.Net.Sockets.NetworkStream aStream)
        {
            byte[] PagingBuffer = new byte[500];
            int NumberOfBytesRead = 0;
            NumberOfBytesRead = aStream.Read(PagingBuffer, 0, PagingBuffer.Length);
            return Encoding.ASCII.GetString(PagingBuffer, 0, NumberOfBytesRead);
        }

        private void SendMessage(System.Net.Sockets.NetworkStream aStream, string aMessage)
        {
            byte[] PagingBuffer = Encoding.ASCII.GetBytes(aMessage);
            aStream.Write(PagingBuffer, 0, PagingBuffer.Length);
            CheckResponse(aStream);
        }

        private void CheckResponse(System.Net.Sockets.NetworkStream aStream)
        {
            switch (ReadMessage(aStream).Substring(0, 1))
            {
                case "2":
                    break;
                case "4":
                    throw new Exception("TSnpp.Send : Connection terminated");
                case "5":
                    throw new Exception("TSnpp.Send : Failed, but connection intact");
                default:
                    throw new Exception("TSnpp.Send : Unknown Response");
            }
        }
    }
}
