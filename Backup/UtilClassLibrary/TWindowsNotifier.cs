﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CtsClassLibrary;

namespace UtilClassLibrary
{    
    class TWindowsNotifierEmail
    {                
        TCtsLogger fLogger;
        EMailSetup Setup = new EMailSetup();
        EMailMessage SendErrorMessage = new EMailMessage();
        EMailMessage StillRunningMessage = new EMailMessage();

        public TWindowsNotifierEmail(TCtsLogger aLogger)
        {
            fLogger = aLogger;
            TCtsContext Context = TCtsContext.Instance;
            TCtsDbIni Ini = new TCtsDbIni(Context.ConfigTableName);

            const string IniSect = "TWindowsNotifierEmail";
            Setup.Host =      Ini.ReadString( Context.ContextKey, IniSect, "Server", "");
            Setup.Port =      Ini.ReadInteger(Context.ContextKey, IniSect, "Port", 0);
            Setup.UserName =  Ini.ReadString( Context.ContextKey, IniSect, "User", "");
            Setup.Password =  Ini.ReadString( Context.ContextKey, IniSect, "Password", "");
            Setup.EnableSsl = Ini.ReadBool(   Context.ContextKey, IniSect, "EnableSsl", false);

            SendErrorMessage.ToAddr =   Ini.ReadString(Context.ContextKey, IniSect, "SendToError", "");
            SendErrorMessage.FromAddr = Ini.ReadString(Context.ContextKey, IniSect, "SenderError", "");
            SendErrorMessage.Subject =  Ini.ReadString(Context.ContextKey, IniSect, "SubjectError", "This process is in distress");
            SendErrorMessage.Message =  Ini.ReadString(Context.ContextKey, IniSect, "MessageError", "This process is in distress");

            StillRunningMessage.ToAddr =   Ini.ReadString(Context.ContextKey, IniSect, "SendToStill", "");
            StillRunningMessage.FromAddr = Ini.ReadString(Context.ContextKey, IniSect, "SenderStill", "");
            StillRunningMessage.Subject =  Ini.ReadString(Context.ContextKey, IniSect, "SubjectStill", "This process is still running properly");
            StillRunningMessage.Message =  Ini.ReadString(Context.ContextKey, IniSect, "MessageStill", "This process is still running properly");
        }

        public void SendError()
        {
            new TEmail(fLogger, Setup).Send(SendErrorMessage);
        }

        public void SendStillRunning()
        {
            new TEmail(fLogger, Setup).Send(StillRunningMessage);
        }
    }

    class TExceptionRing
    {
        private DateTime[] FExceptionList;
        private int FMaxException;
        private int FIndex = -1;

        public TExceptionRing(int aMaxException)
        {
            FMaxException = aMaxException;
            FExceptionList = new DateTime[FMaxException];
            DateTime dt = DateTime.Now.AddDays(-1);
            for (int Item = 0; Item < FMaxException; Item++)
            {
                FExceptionList[Item] = dt;
            }
        }

        private int Next(int aIndex)
        {
            int Result = aIndex + 1;
            if ( Result > FMaxException - 1 ) { Result = 0; }
            return Result;
        }

        private int Pred(int aIndex)
        {
            int Result = aIndex - 1;
            if (Result < 0) { Result = FMaxException - 1; }
            return Result;
        }

        public DateTime Get ()
        {
            return FExceptionList[Next(FIndex)];
        }

        public void Add(DateTime aTime)
        {
            FIndex = Next(FIndex);
            FExceptionList[FIndex] = aTime;
        }
    }

    public class TWindowsNotifier: INotifierForWinSvc
    {
        TCtsLogger fLogger;
        TWindowsNotifierEmail EMail;
        System.TimeSpan TimePeriod;
        System.TimeSpan EMailPeriod;
        DateTime LastMessage;
        DateTime LastCheck;
        TExceptionRing ExceptionRing;
        bool Functional;

        public TWindowsNotifier(TCtsLogger aLogger)
        {
            fLogger = aLogger;
            TCtsContext Context = TCtsContext.Instance;
            TCtsDbIni Ini = new TCtsDbIni(Context.ConfigTableName);
            const string IniSect = "TCtsWindowsNotifier";

            int temp = Ini.ReadInteger(Context.ContextKey, IniSect, "EMailPeriod", 30);
            EMailPeriod = new TimeSpan(0, temp, 0);
            temp = Ini.ReadInteger(Context.ContextKey, IniSect, "TimePeriod",  30);
            TimePeriod = new TimeSpan(0, temp, 0);
            int fMax =    Ini.ReadInteger(Context.ContextKey, IniSect, "MaxExceptions", 10);
            Functional =  Ini.ReadBool   (Context.ContextKey, IniSect, "Functional", false);

            LastCheck = DateTime.Now.Date;
            LastMessage = DateTime.Now.AddHours(-24.0);
            ExceptionRing = new TExceptionRing(fMax);
            EMail = new TWindowsNotifierEmail(fLogger);
        }
        
        public void Execute(bool aException)
        {
            if (Functional)
            {
                if (aException) 
                {
                    CheckExceptions();
                }

                CheckStillRunning();
            }
        }

        private void CheckExceptions()
        {
            DateTime fNow = DateTime.Now;
            ExceptionRing.Add(fNow);
            DateTime fThen = ExceptionRing.Get();
            if (fThen != null)
            {
                if (fThen + TimePeriod > fNow)
                {
                    if (LastMessage + EMailPeriod < fNow)
                    {
                        EMail.SendError();
                        LastMessage = fNow;
                    }
                }
            }
        }

        private void CheckStillRunning()
        {
            DateTime fNow = DateTime.Now.Date;
            if (fNow > LastCheck )
            {
                EMail.SendStillRunning();                
                LastCheck = fNow;
            }
        }
    }
}
