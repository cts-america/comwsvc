﻿using System;
using System.IO;
using System.Net;

namespace UtilClassLibrary
{
    public class DownloadUnsuccessfulException : System.ApplicationException
    {
        public DownloadUnsuccessfulException(string message) { }
        // Constructor needed for serialization 
        // when exception propagates from a remoting server to the client.
        protected DownloadUnsuccessfulException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }

    public class FTPDownloadInformation
    {
        public string FileName { get; set; }
        public string ServerFileLocation { get; set; }
        public string ClientFileLocation { get; set; }
    }

    public class tFTPClient
    {
        private string ServerConnectionString { get; set; }
        private ICredentials Credentials;

        public tFTPClient(string aServerConnectionString,
                          string aUserName, string aUserPassword)
        {
            ServerConnectionString = aServerConnectionString;
            Credentials = new NetworkCredential(aUserName, aUserPassword);
        }

        public Boolean IsServerOnline()
        {
            FtpWebRequest Request;
            FtpWebResponse Response;

            Request = (FtpWebRequest)WebRequest.Create(ServerConnectionString);
            Request.Credentials = Credentials;
            Request.Method = WebRequestMethods.Ftp.ListDirectory;
            try
            {
                Response = (FtpWebResponse)Request.GetResponse();
                return true;
            }
            catch (WebException)
            {
                return false;
            }
        }

        public void DownloadFile(object FtpDlInfo)
        {
            FTPDownloadInformation DownloadInfo = (FTPDownloadInformation)FtpDlInfo;
            string aClientFileLocation = DownloadInfo.ClientFileLocation;
            string aFileName = DownloadInfo.FileName;
            string aServerFileLocation = DownloadInfo.ServerFileLocation;

            FtpWebRequest Request;
            BinaryReader StreamReader;
            BinaryWriter FileWriter;
            FtpWebResponse Response;
            FileInfo Fi;
            Stream ResponseStream;
            Boolean Complete = false;
            Fi = new FileInfo(aClientFileLocation + aFileName);
            if (!Fi.Exists)
            {
                FileStream fs = Fi.Create();
                fs.Close();
                Fi.Refresh();
            }

            Request = (FtpWebRequest)WebRequest.Create(ServerConnectionString +
                                                       aServerFileLocation +
                                                       aFileName);
            Request.Method = WebRequestMethods.Ftp.DownloadFile;
            Request.ContentOffset = Fi.Length;
            Request.Credentials = Credentials;
            try
            {
                Response = (FtpWebResponse)Request.GetResponse();
                ResponseStream = Response.GetResponseStream();
                StreamReader = new BinaryReader(ResponseStream);
                FileWriter = new BinaryWriter(Fi.OpenWrite());
                FileWriter.Seek((int)Fi.Length, SeekOrigin.Begin);
                while (!Complete)
                {
                    byte[] buffy = StreamReader.ReadBytes(50);
                    FileWriter.Write(buffy);
                    if (buffy.Length != 50)
                        Complete = true;
                }
                FileWriter.Close();
                StreamReader.Close();
                Response.Close();
            }
            catch (WebException E)
            {
                if (E.Status == WebExceptionStatus.ConnectFailure)
                    throw new DownloadUnsuccessfulException("No Connection Established");
                if (E.Status == WebExceptionStatus.ConnectionClosed)
                    throw new DownloadUnsuccessfulException("Got Disconnected");
                if (E.Status == WebExceptionStatus.ProtocolError)
                    throw new DownloadUnsuccessfulException("Got Disconnected");
            }
            /*finally
            {
            
            }*/
        }
    }
}
