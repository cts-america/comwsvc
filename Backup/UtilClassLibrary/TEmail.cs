﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Mail;

using CtsClassLibrary;

namespace UtilClassLibrary
{
    public class EMailSetup
    {
        public EMailSetup() { }

        private string fHost = "";
        public string Host { get{return fHost;} set{fHost = value;} }
        private int fPort = 0;
        public int Port { get { return fPort; } set { fPort = value; } }
        private bool fEnableSsl = false;
        public bool EnableSsl { get { return fEnableSsl; } set { fEnableSsl = value; } }
        private string fUserName = "";
        public string UserName { get { return fUserName; } set { fUserName = value; } }
        private string fPassword = "";
        public string Password { get { return fPassword; } set { fPassword = value; } }
    }

    public class EMailMessage
    {
        public EMailMessage() { }

        private string fToAddr = "";
        public string ToAddr { get { return fToAddr; } set { fToAddr = value; } }
        private string fFromAddr = "";
        public string FromAddr { get { return fFromAddr; } set { fFromAddr = value; } }
        private string fMessage = "";
        public string Message { get { return fMessage; } set { fMessage = value; } }
        private string fSubject = "";
        public string Subject { get { return fSubject; } set { fSubject = value; } }
    }

    public class TEmail
    {
        private EMailSetup Setup;

        public TCtsLogger Logger = null;

        public TEmail(TCtsLogger aLogger, EMailSetup aSetup)
        {
            Logger = aLogger;
            Setup = aSetup;
        }

        public void Send(EMailMessage EM)
        {
            MailMessage message = new MailMessage();
            try
            {
                SmtpClient mail = new SmtpClient(Setup.Host, Setup.Port);

                message.From = new MailAddress(EM.FromAddr, EM.FromAddr, System.Text.Encoding.UTF8);
                message.To.Add(EM.ToAddr);
                message.Body = EM.Message;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = EM.Subject;
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                mail.EnableSsl = Setup.EnableSsl;
                mail.DeliveryMethod = SmtpDeliveryMethod.Network;

                if ((Setup.UserName != "")
                  | (Setup.Password != ""))
                {
                    mail.Credentials = new System.Net.NetworkCredential(Setup.UserName, Setup.Password);
                }
                else
                    mail.UseDefaultCredentials = true;

                mail.Send(message);

            }
            catch (Exception E)
            {
                if (Logger != null)
                { Logger.Write("TEmail.Send " + E.Message, TLogLevel.llException); }
                else
                { throw new Exception("TEmail.Send " + E.Message); }
            }
            finally
            {
                message.Dispose();
            }
        }
    }
}
