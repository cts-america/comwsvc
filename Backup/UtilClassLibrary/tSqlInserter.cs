﻿using System;

using CtsClassLibrary;

namespace UtilClassLibrary
{

    public class tSqlInserter
    {
        private CtsClassLibrary.TCtsQuery Query;
        private string InsertSql;
        private string Values;
        private bool FirstValue;
        public tSqlInserter(string aConnectionString, string aTableName)
        {
            Query = new CtsClassLibrary.TCtsQuery(aConnectionString, false);
            InsertSql = "INSERT into :Table(";
            Values = ") Values ('";
            InsertSql = InsertSql.Replace(":Table", aTableName);
            FirstValue = true;
        }
        public void AddNameValue(string aName, string aValue)
        {
            if (FirstValue)
            {
                InsertSql += aName;
                Values += aValue;
                FirstValue = false;
            }
            else
            {
                InsertSql += ", " + aName;
                Values += "', '" + aValue;
            }
        }
        public void Execute()
        {
            InsertSql += Values + "')";
            Query.ExecuteQuery(InsertSql);
            Query.Close();
        }
    }
}
