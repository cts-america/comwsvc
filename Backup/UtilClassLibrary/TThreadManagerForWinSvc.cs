﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilClassLibrary
{
    public interface IThreadManagerForWinSvc
    {
        void Pause();
        void Resume();
        void Done();
    }
}
