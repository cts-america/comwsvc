﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceProcess;

using CtsClassLibrary;

namespace UtilClassLibrary
{
    public class TCtsServiceBase: ServiceBase
    {
        public TCtsServiceBase() : base() { }

        public TCtsServiceBase(string aClassName): base()
        {
            //aClassName is the section name in the ini file (aka rootname)
            new TCtsContext(TAppType.atWebSvc, aClassName);
            ServiceName = this.ToString();
        }

        protected IThreadManagerForWinSvc Mgr = null;
        protected TCtsLogger Logger = null;

        protected virtual void SetThreadManager()
        {
            //VS designer cannot create an abstract class so we do this.
            //You must override to create your supporting class of IThreadManagerForWinSvc 
            //Mgr = new TThreadManagerForWinSvc();
        }

        /*    
        procedure ServiceExecute(Sender: TService);
        */

        protected override void OnContinue()
        {
            Logger.Write(ServiceName + ".OnContinue", TLogLevel.llFlow);
            Mgr.Resume();
        }

        protected override void OnPause()
        {
            Logger.Write(ServiceName + ".OnPause", TLogLevel.llFlow);
            Mgr.Pause();
        }

        protected override void OnStart(string[] args)
        {
            Logger = new TCtsLogger(TLogHelper.lhFile);
            Logger.Write(ServiceName + ".Start", TLogLevel.llFlow);
            try
            {
                SetThreadManager();
                Logger.Write(ServiceName + ".Start done", TLogLevel.llFlow);
                Mgr.Resume();
            }
            catch(Exception e)
            {
                Logger.Write(ServiceName + ".Start " + e.Message, TLogLevel.llFlow);
            }
        }

        protected override void OnStop()
        {
            Logger.Write(ServiceName + ".Stop", TLogLevel.llFlow);
            Mgr.Done();
        }
    }
}
