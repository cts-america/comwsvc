﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CtsClassLibrary;

namespace UtilClassLibrary
{
    public enum TapiParity { tpEven, tpOdd }

    public class TapiSetup
    {
        public TapiSetup() { }

        private int fComNumber;
        public int ComNumber { get { return fComNumber; } set { fComNumber = value; } }
        private int fBaud;
        public int Baud { get { return fBaud; } set { fBaud = value; } }
        private int fBuffy;
        public int BufferSize { get { return fBuffy; } set { fBuffy = value; } }
        private TapiParity fParity;
        public TapiParity Parity { get { return fParity; } set { fParity = value; } }
        private int fDataBits;
        public int DataBits { get { return fDataBits; } set { fDataBits = value; } }
        private string fModemHangup;
        public string ModemHangup { get { return fModemHangup; } set { fModemHangup = value; } }
        private string fModemInit;
        public string ModemInit { get { return fModemInit; } set { fModemInit = value; } }
    }

    public class TapiMessage
    {
        public TapiMessage() { }

        private string fPagerID = "";
        public string PagerID { get { return fPagerID; } set { fPagerID = value; } }
        private string fPhone = "";
        public string Phone { get { return fPhone; } set { fPhone = value; } }
        private string fMessage = "";
        public string Message { get { return fMessage; } set { fMessage = value; } }
    }

    public class TTapi
    {
        private TapiSetup Setup;

        public TCtsLogger Logger = null;

        public TTapi(TCtsLogger aLogger, TapiSetup aSetup)
        {
            Logger = aLogger;
            Setup = aSetup;
        }

        public void Send(TapiMessage SM)
        {
            try
            {
                //todo mail.Send(TapiMessage);
            }
            catch (Exception E)
            {
                if (Logger != null)
                { Logger.Write("TTapi.Send " + E.Message, TLogLevel.llException); }
                else
                { throw new Exception("TTapi.Send " + E.Message); }
            }
        }
    }
}
