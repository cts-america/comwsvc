﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;

using CtsClassLibrary;

namespace UtilClassLibrary
{
    public abstract class PollingThread
    {
        private Timer tmr;
        private int fPollingPeriod = 0;
        protected TCtsLogger fLogger = null;

        public PollingThread(int aPollingPeriod, TCtsLogger aLogger)
        {
            fLogger = aLogger;
            fPollingPeriod = aPollingPeriod * 1000;
            tmr = new Timer(Execute, this, -1, 0);
            //(use Timeout.Infinite for a one-off callback)
        }

        protected abstract void Event();

        public void Pause() { tmr.Change(-1, 0); }

        public void Resume() { tmr.Change(fPollingPeriod, 0); }

        static void Execute(object aInstance)
        {
            if (aInstance is PollingThread) 
            {
                PollingThread P = (PollingThread)aInstance;
                P.tmr.Change(-1, 0);
                try
                {
                    P.Event();
                }
                catch (Exception e)
                {
                    P.fLogger.Write("PollingThread.Execute."+ P.ToString() +" "+ e.Message, TLogLevel.llException);
                }
                finally
                {
                    P.tmr.Change(P.fPollingPeriod, 0);
                }
            }
        }

        public void GoAndDie()
        {
            tmr.Dispose();
        }

    }
}
