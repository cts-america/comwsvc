﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CtsClassLibrary;

namespace UtilClassLibrary
{
    public class TermSetup
    {
        public TermSetup() { }

        private int fComNumber;
        public int ComNumber { get { return fComNumber; } set { fComNumber = value; } }
        private int fBaud;
        public int Baud { get { return fBaud; } set { fBaud = value; } }
        private int fBuffy;
        public int BufferSize { get { return fBuffy; } set { fBuffy = value; } }
    }

    public class TermMessage
    {
        public TermMessage() { }

        private string fPagerNo = "";
        public string PagerNo { get { return fPagerNo; } set { fPagerNo = value; } }
        private string fMessage = "";
        public string Message { get { return fMessage; } set { fMessage = value; } }
    }

    public class TTerm
    {
        private TermSetup Setup;

        public TCtsLogger Logger = null;

        public TTerm(TCtsLogger aLogger, TermSetup aSetup)
        {
            Logger = aLogger;
            Setup = aSetup;
        }

        public void Send(TermMessage SM)
        {
            try
            {
                //todo mail.Send(TermMessage);
            }
            catch (Exception E)
            {
                if (Logger != null)
                { Logger.Write("TTerm.Send " + E.Message, TLogLevel.llException); }
                else
                { throw new Exception("TTerm.Send " + E.Message); }
            }
        }
    }
}
