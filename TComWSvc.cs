﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using CtsClassLibrary;
using UtilClassLibrary;

namespace ComWSvc
{
    public partial class ComWSvc : TCtsServiceBase
    {

        public ComWSvc(): base("ComWSvc")
        {            
            InitializeComponent();
        }

        protected override void SetThreadManager()
        {
            Mgr =  (UtilClassLibrary.IThreadManagerForWinSvc) new TComThreadMgr(Logger);
        }

    }
}
