﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

using UtilClassLibrary;
using CtsClassLibrary;

namespace ComWSvc
{
    class SnppPaging : PagingThread
    {
        SnppSetup Setup = new SnppSetup();

        public string LocalAddress 
        {
            get { return Setup.LocalAddress; }
            set { Setup.LocalAddress = value; }
        }

        public SnppPaging(int aPollingPeriod, TCtsLogger aLogger)
            : base(aPollingPeriod, aLogger)
        {
            fLogger.Write("SnppPaging.Create", TLogLevel.llFlow);
            Setup.Port = 0;
        }

        private bool Post(string aPager, string aServer, string aMessage)
        {
            try
            {
                SnppMessage SM = new SnppMessage();
                SM.PagerNo = aPager;
                SM.Address = aServer.Substring(0, aServer.IndexOf(":"));
                SM.Port = aServer.Substring(aServer.IndexOf(":") + 1);
                SM.Message = aMessage;

                new TSnpp(fLogger, Setup).Send(SM);
				fLogger.Write("SnppPaging.Post returning true", TLogLevel.llFlow);
                return true;
            }
            catch(Exception E)
            {
                fLogger.Write("SnppPaging.Post" + E.Message, TLogLevel.llException);
                return false;
            }
        }

        protected override void Event()
        {
            fLogger.Write("SnppPaging.Event", TLogLevel.llFlow);
            string Deletes = "";
            string Updates = "";
            try
            {
                TCtsQuery Query = new TCtsQuery("Mct_Web");
                SqlDataReader Reader = Query.ExecuteProc("dbo.spsc_GetSNPPPages");
                while (Reader.Read())
                {
                    bool fSent = Post((string)Reader["PAGER"],
                                      (string)Reader["MAIL_SERVER"],
                                      (string)Reader["MESSAGE"]);
                    
                    if (IsLogMsgs) 
                    { 
                        TransLog( fSent ? "CPage" : "XPage",
                           (string)Reader["PAGEDBY"],
                           (string)Reader["MESSAGE"],
                           (string)Reader["PAGER"],
                           (string)Reader["CallNo"],
                           (string)Reader["PAGEDBY"]);
                    }

                    if (fSent)
                    { Deletes += TXml.Encode("row", TXml.Encode("Uniquekey", (string)Reader["Uniquekey"])); }
                    else
                    { Updates += TXml.Encode("row", TXml.Encode("Uniquekey", (string)Reader["Uniquekey"])); }

                }
                TCtsQuery Proc = new TCtsQuery("Mct_Web");
				fLogger.Write("SnppPaging.Event deletes: " + Deletes, TLogLevel.llFlow);
				fLogger.Write("SnppPaging.Event updates: " + Updates, TLogLevel.llFlow);
                Proc.ParmByName("@DeleteXml", SqlDbType.VarChar, TXml.Encode("Delete", Deletes));
                Proc.ParmByName("@UpDateXml", SqlDbType.VarChar, TXml.Encode("Update", Updates));
                Proc.ExecuteProc("dbo.spsc_UpdateDeletePages");
                Proc.Close();

            }
            catch(Exception e)
            {
                fLogger.Write("SnppPaging.Event " + e.Message, TLogLevel.llException);
                fLogger.Write("SnppPaging.Event These were sent but not deleted: "+ Updates, TLogLevel.llTransAction);
                fLogger.Write("SnppPaging.Event These were not sent and not updated: "+ Deletes, TLogLevel.llTransAction);
            }
        }
    }
}
